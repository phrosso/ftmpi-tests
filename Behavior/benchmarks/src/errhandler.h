#include <mpi.h>
#include <mpi-ext.h>
#include <stdio.h>

/// Triggers whenever a MPI error occurs
void errorHandlerFunction(MPI_Comm *pcomm, int *perr, ...) {
  int len, ec;
  char errstr[MPI_MAX_ERROR_STRING];
  MPI_Error_string(*perr, errstr, &len);
  MPI_Error_class(*perr, &ec);
  printf("Error: %s\n", errstr);
  printf("Errorclass: %d\n", ec);
}


