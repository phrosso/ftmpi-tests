#include "mpi.h"
#include <cstdio>
#include <cstdlib>
#include "errhandler.h"
#include <signal.h>
#include <unistd.h>

#if COMPILE_WITH_ULFM
#include <mpi-ext.h>
#endif

#define MSGLEN 1024

int main(int argc, char *argv[]) {
  int rank, size, i, provided;
  int buffer[MSGLEN];
  MPI_Status status;

  if (argc != 3) {
    printf("Invalid number of arguments. Use: mpirun -n 2 --<recovery-flag> "
           "<program> <failed-process=0|1> "
           "<type=0/blocking|1/non-blocking>\n");
  }

  int failed_process = atoi(argv[1]);
  int type = atoi(argv[2]);

  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // Set error handling to return an error
  MPI_Errhandler errh;
  MPI_Comm_create_errhandler(errorHandlerFunction, &errh);
  MPI_Comm_set_errhandler(MPI_COMM_WORLD,errh);

  if (rank == 0) {
    if (failed_process == 0)
      raise(SIGKILL);

    for (i = 0; i < MSGLEN; i++)
      buffer[i] = i;
  } else if (rank == 1) {
    if (failed_process == 1)
      raise(SIGKILL);

    for (i = 0; i < MSGLEN; i++)
      buffer[i] = -1;
  }

  switch (type) {
  case 0:
    MPI_Bcast(buffer, MSGLEN, MPI_INT, 0, MPI_COMM_WORLD);
    break;
  case 1:
    MPI_Request req;
    MPI_Ibcast(buffer, MSGLEN, MPI_INT, 0, MPI_COMM_WORLD, &req);
    if (failed_process == -1)
      MPI_Wait(&req, &status);
    else
      MPI_Request_free(&req);
    break;
  default:
    printf("Invalid type of Bcast operation\n");
    exit(1);
    break;
  }

  if (rank == 1) {
    for (i = 0; i < MSGLEN; i++) {
      if (buffer[i] != i)
        printf("Error: buffer[%d] = %d but is expected to be %d\n", i,
               buffer[i], i);
    }
  }

  MPI_Finalize();
  return 0;
}
