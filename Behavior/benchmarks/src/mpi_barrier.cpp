#include "mpi.h"
#include <cstdio>
#include <cstdlib>
#include "errhandler.h"
#include <signal.h>
#include <unistd.h>

#if COMPILE_WITH_ULFM
#include <mpi-ext.h>
#endif

int main(int argc, char *argv[]) {
  int rank, size, i, provided;
  MPI_Request req;
  MPI_Status status;

  if (argc != 3) {
    printf("Invalid number of arguments. Use: mpirun -n 2 --<recovery-flag> "
           "<program> <failed-process=0|1> "
           "<type=0/blocking|1/non-blocking>\n");
  }

  int failed_process = atoi(argv[1]);
  int type = atoi(argv[2]);

  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // Set error handling to return an error
  MPI_Errhandler errh;
  MPI_Comm_create_errhandler(errorHandlerFunction, &errh);
  MPI_Comm_set_errhandler(MPI_COMM_WORLD,errh);

  if (rank == 0)
    if (failed_process == 0)
      raise(SIGKILL);
  if (rank == 1)
    if (failed_process == 1)
      raise(SIGKILL);

  if (type == 0) {
    MPI_Barrier(MPI_COMM_WORLD);
  } else if (type == 1) {
    MPI_Ibarrier(MPI_COMM_WORLD, &req);
    if (failed_process == -1)
      MPI_Wait(&req, &status);
    else
      MPI_Request_free(&req);
  }

  MPI_Finalize();
  return 0;
}
