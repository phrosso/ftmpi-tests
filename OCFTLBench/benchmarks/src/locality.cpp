#include <algorithm>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <functional>
#include <mpi.h>
#include <signal.h>
#include <thread>
#include <vector>
#include <random>
#include <iostream>

#include "ft.h"

using namespace std;

// This program tests the notification system to receive the information about
// fault tolerance events with multiple callbacks
int main(int argc, char *argv[]) {
  int provided, rank, size;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  MPI_Comm device_comm = MPI_COMM_WORLD;
  MPI_Comm_rank(device_comm, &rank);
  MPI_Comm_size(device_comm, &size);

  FaultTolerance *heartbeat = new FaultTolerance(1000, 10000, device_comm);

  printf("before barrier\n");

  MPI_Barrier(MPI_COMM_WORLD);

  printf("After barrier\n");
  int processes_to_kill = std::stoi(argv[1]);
  int processes_per_node = std::stoi(argv[2]);
  int type = std::stoi(argv[3]);
  std::chrono::system_clock::time_point begin, first_failure, all_failures;

  bool alive = true;

  // Kill processes
  if (type == 0) { // sequential kills
    if (rank < processes_to_kill) {
      printf("Ending process %d\n", rank);
      alive = false;
    }
  } else if (type == 1) {
    if (rank % processes_per_node == 0) {
      if (rank < (processes_to_kill * processes_per_node)) {
        printf("Ending process %d\n", rank);
        alive = false;
      }
    }
  }
  
  MPI_Barrier(MPI_COMM_WORLD);  
  if (alive == true) {
    // Start heartbeat on remaining processes
    heartbeat->start_hb = true;

    // Start Clock
    begin = std::chrono::system_clock::now();

    std::this_thread::sleep_for(std::chrono::microseconds(2000));
    // Watch the detection and propagation
    while(1) {
      // First failure (if total failures == 1, loop will break in next if statement)
      if (heartbeat->getFailures() == 1) {
        first_failure = std::chrono::system_clock::now();
      }
      // All failures (break the loop)
      if (heartbeat->getFailures() >= heartbeat->getTotalFailures()) { 
        break;
      }
      std::this_thread::sleep_for(std::chrono::microseconds(10));
    }

    // Stop counting
    all_failures = std::chrono::system_clock::now();

    auto first = std::chrono::duration_cast<std::chrono::microseconds>(first_failure - begin).count();
    auto all = std::chrono::duration_cast<std::chrono::microseconds>(all_failures - begin).count();
    // Register the time to get all failures
    std::this_thread::sleep_for(std::chrono::seconds(2)); 
    int bc_messages_sent = heartbeat->bc_messages_sent;
    int bc_messages_recv = heartbeat->bc_messages_recv;
    fprintf(stderr, "%d %ld %ld %d %d\n", rank, first, all, bc_messages_sent, bc_messages_recv);
  }
  MPI_Barrier(MPI_COMM_WORLD);

  // Finish
  if (alive == false)
    heartbeat->start_hb = true;
  delete heartbeat;
  MPI_Finalize();
  return EXIT_SUCCESS;
}
