#!/bin/bash

module purge
module load "cmake/3.17.3"
module load "gcc/7.4"
export CC=gcc
export CXX=g++
export PATH=/scratch/parceirosbr/brcluster/pedro.rosso/custom_mpi/mpich-noucx/bin:$PATH
export LD_LIBRARY_PATH=/scratch/parceirosbr/brcluster/pedro.rosso/custom_mpi/mpich-noucx/lib:$LD_LIBRARY_PATH
export LD_RUN_PATH=/scratch/parceirosbr/brcluster/pedro.rosso/custom_mpi/mpich-noucx/lib:$LD_RUN_PATH

## Heartbeat configs
export OMPCLUSTER_FT_DISABLE=0
export OMPCLUSTER_WRAPPERS_DISABLE=1
export OMPCLUSTER_HB_TIMESTEP=1
export OMPCLUSTER_HB_PERIOD=100
export OMPCLUSTER_HB_TIMEOUT=1000
export FTLIB_WHICH_BC=0

j=${1}

export FTLIB_TOTAL_FAILURES=$j
export MPIEXEC_TIMEOUT=60

for i in {1..10}; do  
  echo "Running sample $i of $j failures for sequential failures"
  export FTLIB_RING_SHUFFLE=1
  mpirun -np 480 -iface ib0 --disable-auto-cleanup ./locality $j 24 0 >> shuffle.out 2>> shuffle.err; 
  export FTLIB_RING_SHUFFLE=0
  mpirun -np 480 -iface ib0 --disable-auto-cleanup ./locality $j 24 0 >> standard.out 2>> standard.err;
done
mv *.out results/locality/sequential/$j/
mv *.err results/locality/sequential/$j/

for i in {1..10}; do
  echo "Running sample $i of $j failures for random failures"
  export FTLIB_RING_SHUFFLE=1
  mpirun -np 480 -iface ib0 --disable-auto-cleanup ./locality $j 24 1 >> shuffle.out 2>> shuffle.err;   
  export FTLIB_RING_SHUFFLE=0
  mpirun -np 480 -iface ib0 --disable-auto-cleanup ./locality $j 24 1 >> standard.out 2>> standard.err;
done
mv *.out results/locality/random/$j/
mv *.err results/locality/random/$j/



























